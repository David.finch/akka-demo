﻿using System;
using Akka.Actor;

namespace AkkaApiDemo.Actors
{
    public class EchoActor : ReceiveActor {

        public readonly String name;
        public int echoCount;

        public EchoActor(String name) {
            this.name = name;


            Receive<EchoMessage>(message => {
                echoCount++;
                Sender.Tell(new EchoMessage(name, message.message));
            });
        }

        public static Props Props(String name) {

            return Akka.Actor.Props.Create<EchoActor>(() => new EchoActor(name));
        }

        public class EchoMessage {
            public readonly String senderName;
            public readonly String message;

            public EchoMessage(String senderName, String message) {
                this.senderName = senderName;
                this.message = message;
            }
        }
    }
}
