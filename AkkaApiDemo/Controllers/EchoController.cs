﻿using System;
using System.Threading.Tasks;
using Akka.Actor;
using AkkaApiDemo.Actors;
using Microsoft.AspNetCore.Mvc;
using static AkkaApiDemo.Startup;

namespace AkkaApiDemo.Controllers
{
    [ApiController]
    [Route("echo")]
    public class EchoController : ControllerBase
    {
        private readonly IActorRef echoActor;

        public EchoController(EchoActorSystemProvider echoActorSystemProvider) {
            echoActor = echoActorSystemProvider();
        }

        [HttpPost]
        public async Task<IActionResult> Post(String name, String message)
        {
            var result = await echoActor.Ask(new EchoActor.EchoMessage(name, message));
            switch (result) {
                case EchoActor.EchoMessage echo:
                    return new OkObjectResult(echo.message);
                 default:
                    return BadRequest();
            }
        }
    }
}
