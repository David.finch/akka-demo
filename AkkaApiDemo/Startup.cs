﻿using Akka.Actor;
using AkkaApiDemo.Actors;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using IApplicationLifetime = Microsoft.Extensions.Hosting.IApplicationLifetime;

namespace AkkaApiDemo
{
    public class Startup
    {
        // Used to provide a Single Actor System
        public delegate IActorRef EchoActorSystemProvider();

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // Create the actor system
            services.AddSingleton(_ => ActorSystem.Create("echo"));
            
            // Add the implmentation for the ActorSystemProvider to return only the Singleton actor system from above
            services.AddSingleton<EchoActorSystemProvider>(provider =>
            {
                var actorSystem = provider.GetService<ActorSystem>();
                var echoActor = actorSystem.ActorOf(Props.Create(() => new EchoActor("Jarvis")));
                return () => echoActor;
            });

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime lifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });


            // Start and stop the actor system with the server
            lifetime.ApplicationStarted.Register(() =>
            {
                app.ApplicationServices.GetService<ActorSystem>(); // start Akka.NET
            });
            lifetime.ApplicationStopping.Register(() =>
            {
                app.ApplicationServices.GetService<ActorSystem>().Terminate().Wait();
            });
        }
    }
}
